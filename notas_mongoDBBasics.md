# M001
## CHAPTER 1
m001-student
m001-mongodb-basics

#### Ejercicios
Quiz 
  -> Because it does not utilize tables, rows and columns to organize data.
  -> Because it uses a structured way to store and access data

Quiz 1
  -> MongoDB database is an organized way to store and access data.
  -> MongoDB is a NoSQL database that uses documents to store data in an organized way.

Quiz 2
  -> Collections consist of many documents.

Quiz 3
  -> A field is a unique identifier for a specific datapoint.
  -> Each field has a value associated with it.

Quiz
  -> They are both MongoDB products.
  -> Atlas has many tools and services within it that are built specifically for the MongoDB Database.

Lab -> Yes

## CHAPTER 2
### How does MongoDB store data?
#### Ejercicios
Quiz 
  -> {"name" : "Devi", "age": 19, "major": "Computer Science"}

Quiz 
  -> BSON / JSON / BSON / JSON / JSON / BSON

### Exportar e importar en JSON
mongoimport --uri "<Atlas cluster URI>" 
            --drop=<filename>.json (opcional-> --colection <nombreColeccion>)


mongoexport --uri "<Atlas cluster URI>"
            --collection=<collection name>
            --out=<filename>.json

### Exportar e importar en BSON
mongorestore --uri "<Atlas cluster URI>"
             --drop dump

mongodump --uri "<Atlas cluster URI>"

____

mongoimport --uri="mongodb+srv://<your username>:<your password>@<your cluster>.mongodb.net/sample_supplies" --drop sales.json

mongoexport --uri="mongodb+srv://<your username>:<your password>@<your cluster>.mongodb.net/sample_supplies" --collection=sales --out=sales.json

____
mongorestore --uri "mongodb+srv://<your username>:<your password>@<your cluster>.mongodb.net/sample_supplies"  --drop dump

mongodump --uri "mongodb+srv://<your username>:<your password>@<your cluster>.mongodb.net/sample_supplies"

#### Ejercicios
Quiz 
  -> mongoimport

### Data Explorer
Cluster -> Collections

Namespace - The concatenation of the database name and collection name is called a namespace.

We looked at the sample_training.zips collection and issued the following queries:
{"state": "NY"}
{"state": "NY", "city": "ALBANY"}

#### Ejercicios
Quiz 
  -> {"start station name" : "Howard St & Centre St", "birth year" : 1961}

### Find()
mongo "mongodb+srv://<username>:<password>@<cluster>.mongodb.net/admin" // conecta
show dbs // muestra el listado de las databases
use sample_training // indica que va a trabajar con esa base de datos
show collections // muestra las colecciones de esa database
it iterates through the cursor.
db.zips.find({"state": "NY"}) // como una consulta sql
db.zips.find({"state": "NY"}).count() // cuenta
db.zips.find({"state": "NY", "city": "ALBANY"}) // filtra por ciudad
db.zips.find({"state": "NY", "city": "ALBANY"}).pretty() // formatea el resultado

#### Ejercicios
Quiz 
  -> Iterates through the cursor results
Quiz
  -> It is a fully functioning JavaScript interpreter.
  -> It allows you to interact with your MongoDB instance without using a Graphical User Interface.

## CHAPTER 3
### Insertar nuevos documentos
Cada documento debe tener una única ID
"_id":"ObjectId1a"
Aplica por defecto si no se especifica.

### Ejercicios
Quiz 
  -> You can select a non ObjectId type...
  -> It is automatically... 

### Insert() and errors

Step one: Connect to the Atlas cluster
mongo "mongodb+srv://<username>:<password>@<cluster>.mongodb.net/admin"

Step two: navigate to the database that we need:
use sample_training

Step three, get a random document from the collection:
db.inspections.findOne(); // muestra uno de los documentos

Step four, copy this random document, and try to insert in into the collection:
db.inspections.insert({
      "_id" : ObjectId("56d61033a378eccde8a8354f"),
      "id" : "10021-2015-ENFO",
      "certificate_number" : 9278806,
      "business_name" : "ATLIXCO DELI GROCERY INC.",
      "date" : "Feb 20 2015",
      "result" : "No Violation Issued",
      "sector" : "Cigarette Retail Dealer - 127",
      "address" : {
              "city" : "RIDGEWOOD",
              "zip" : 11385,
              "street" : "MENAHAN ST",
              "number" : 1712
         }
  })

db.inspections.insert({
      "id" : "10021-2015-ENFO",
      "certificate_number" : 9278806,
      "business_name" : "ATLIXCO DELI GROCERY INC.",
      "date" : "Feb 20 2015",
      "result" : "No Violation Issued",
      "sector" : "Cigarette Retail Dealer - 127",
      "address" : {
              "city" : "RIDGEWOOD",
              "zip" : 11385,
              "street" : "MENAHAN ST",
              "number" : 1712
         }
  })

db.inspections.find({"id" : "10021-2015-ENFO", "certificate_number" : 9278806}).pretty()

#### Ejercicios
Quiz 
  -> MongoDB can store duplicate documents in the same...
  -> If a document is inserted without a provided _id value, then the _id field and value will be automatically generated...


### Inserting new Documents - insert()
Insertando varios documentos.

Insert three test documents:
db.inspections.insert([ { "test": 1 }, { "test": 2 }, { "test": 3 } ])

Insert three test documents but specify the _id values:
db.inspections.insert([{ "_id": 1, "test": 1 },{ "_id": 1, "test": 2 },
                       { "_id": 3, "test": 3 }])

Find the documents with _id: 1
db.inspections.find({ "_id": 1 })

Insert multiple documents specifying the _id values, and using the "ordered": false option. // si no está ordenado, aunque se detecte una id duplicada, se inserta en resto
db.inspections.insert([{ "_id": 1, "test": 1 },{ "_id": 1, "test": 2 },
                       { "_id": 3, "test": 3 }],{ "ordered": false })

Insert multiple documents with _id: 1 with the default "ordered": true setting // si está ordenado, en cuanto se detecta una id duplicada, no se inserta más
db.inspection.insert([{ "_id": 1, "test": 1 },{ "_id": 3, "test": 3 }])

View collections in the active db
show collections

Switch the active db to training
use training

View all available databases
show dbs

#### Ejercicios
Quiz 
  -> Todas correctas menos la última.

### Updating documents
En atlas o con shell.

updateMany()
updateOne()

Find all documents in the zips collection where the zip field is equal to "12434".
db.zips.find({ "zip": "12534" }).pretty()

Find all documents in the zips collection where the city field is equal to "HUDSON".
db.zips.find({ "city": "HUDSON" }).pretty()

Find how many documents in the zips collection have the city field equal to "HUDSON".
db.zips.find({ "city": "HUDSON" }).count()

Update all documents in the zips collection where the city field is equal to "HUDSON" by adding 10 to the current value of the "pop" field.
db.zips.updateMany({ "city": "HUDSON" }, { "$inc": { "pop": 10 } }) // $inc incrementa

Update a single document in the zips collection where the zip field is equal to "12534" by setting the value of the "pop" field to 17630.
db.zips.updateOne({ "zip": "12534" }, { "$set": { "pop": 17630 } }) // $set asigna o añade

Update a single document in the zips collection where the zip field is equal to "12534" by setting the value of the "popupation" field to 17630.
db.zips.updateOne({ "zip": "12534" }, { "$set": { "population": 17630 } })

Find all documents in the grades collection where the student_id field is 151 , and the class_id field is 339.
db.grades.find({ "student_id": 151, "class_id": 339 }).pretty()

Find all documents in the grades collection where the student_id field is 250 , and the class_id field is 339.
db.grades.find({ "student_id": 250, "class_id": 339 }).pretty()

Update one document in the grades collection where the student_id is ``250`` *, and the class_id field is 339 , by adding a document element to the "scores" array.
db.grades.updateOne({ "student_id": 250, "class_id": 339 },
                    { "$push": { "scores": { "type": "extra credit", // $push añade un elemento a un array
                                             "score": 100 }
                                }
                     })

#### Ejercicios
Quiz 
  -> None of above.

Quiz 
  -> db.pets.updateMany({ "pet": "cat" },
                   { "$set": { "type": "dangerous",
                               "look": "adorable" }})
  -> db.pets.updateMany({ "pet": "cat" },
                   { "$push": { "climate": "continental",
                                "look": "adorable" } })



### Deleting documents and collections
deleteOne() // elimina una selección que coincide en un documento
deleteMany() // ...que coincide en varios
drop()

-> When all collections are dropped from a database, the database no longer appears in the list of databases when you run show dbs.

db.inspections.find({ "test": 1 }).pretty()
db.inspections.find({ "test": 3 }).pretty()
db.inspections.deleteMany({ "test": 1 })
db.inspections.deleteOne({ "test": 3 })
db.inspection.find().pretty()
show collections
db.inspection.drop() // elimina una colección

#### Ejercicios
Quiz 1 
  -> Yes

Quiz 2
  -> db.villains.drop()

## CHAPTER 4
### Comparasion
$eq EQual to
$gt Greater Than
$gte Greater Than or Equal to
$neq Not Equal to
$lt Less Than
$lte Less Than Or Equal to

#### Ejemplos
mongo "mongodb+srv://<username>:<password>@<cluster>.mongodb.net/admin"
use sample_training
db.trips.find({ "tripduration": { "$lte" : 70 },
                "usertype": { "$ne": "Subscriber" } }).pretty()
db.trips.find({ "tripduration": { "$lte" : 70 },
                "usertype": { "$eq": "Customer" }}).pretty()
db.trips.find({ "tripduration": { "$lte" : 70 },
                "usertype": "Customer" }).pretty()

#### Ejercicios
ej 1. -> db.zips.find({"pop": {"$lt": 1000}}).count()
ej 2. -> db.trips.find({"birth year": {"$gt": 1998}}).count() - db.trips.find({"birth year": {"$eq": 1998}}).count()
ej 3. -> 
  db.routes.find({ "stops": { "$gt": 0 }}).pretty()
  db.routes.find({ "stops": { "$gte": 0 }}).pretty()

### Logic
{"$operator": [{clause1}, {clause2},...]}
$and match all of the specified query clauses
$or at least one of the query clauses is matched
$nor fail to match both fiven clauses

{$not: {clause}}
$not negates the query requirement

#### Ejemplos
db.routes.find({ "$and": [ { "$or" :[ { "dst_airport": "KZN" },{ "src_airport": "KZN" }] }, { "$or" :[ { "airplane": "CR2" },{ "airplane": "A81" } ] }
                         ]}).pretty()

#### Ejercicios
ej 4. -> db.inspections.find({"$and": [{"result": {"$eq": "Out of Business"}}, {"sector": {"$eq": "Home Improvement Contractor - 100"}}]}).count()

ej 5. -> la opción que no tiene .not ni .pretty

ej 6. (lab1) -> [al hacerlo en el ide, aplicar primero db.zips.updateMany({ "city": "HUDSON" }, { "$inc": { "pop": -10 } })   para eliminar cambios anteriores si se han hecho los ejemplos con el vídeo] 
  db.zips.find({"$nor": [{"pop": {"gt": 100000}}, {"pop": {"$lt": 5000}}]}).count();

ej 8. (lab2) -> 
  db.companies.find({"$or": [{"$and": [{"founded_year": 2004}, {"$or": [{"category_code": "web"}, {"category_code": "social"}]}]}, {"$and": [{"founded_month": 10}, {"$or": [{"category_code": "web"}, {"category_code": "social"}]}]}]}).count()  // 149

  Opción campus -> db.companies.find({ "$and": [
                        { "$or": [ { "founded_year": 2004 },
                                   { "founded_month": 10 } ] },
                        { "$or": [ { "category_code": "web" },
                                   { "category_code": "social" }]}]}).count()


### Expressive query operator
$expr 
Allows the use of aggregation expressions within the query language.
Alows us to use variables and conditional statements.
{$expr: {expression}}

$ -> Denotes the use of an operator & addresses the field value.

Syntax for comparison operators using aggregation:
{operator: {field, value}}

#### Ejemplos
Find all documents where the trip started and ended at the same station:
db.trips.find({ "$expr": { "$eq": [ "$end station id", "$start station id"] }
              }).count()

Find all documents where the trip lasted longer than 1200 seconds, and started and ended at the same station:
db.trips.find({ "$expr": { "$and": [ { "$gt": [ "$tripduration", 1200 ]},
                         { "$eq": [ "$end station id", "$start station id" ]}
                       ]}}).count()

#### Ejercicios
ej 9. (lab $expr) -> db.companies.find({$expr: {"$eq": ["$permalink", "$twitter_username"]}}).count()

### Array operators
$push 
allow us to add an element to an array or turn a field into an array field if it was previously a different type.

$size 
Returns a cursor with all documents where the specified array field is exactly the given length.
{arrayField: {$size: nunber}}

$all
Returns a cursor with all documents which the specified array field contains all
the given elements regardless of their order in the array.

Querying an array field using -> An array returns only exact array matches. A single element
wil return all documents where the specified array field contains this given element.

#### Ejemplos
Find all documents with exactly 20 amenities which include all the amenities listed in the query array:
db.listingsAndReviews.find({ "amenities": {
                                  "$size": 20,
                                  "$all": [ "Internet", "Wifi",  "Kitchen",
                                           "Heating", "Family/kid friendly",
                                           "Washer", "Dryer", "Essentials",
                                           "Shampoo", "Hangers",
                                           "Hair dryer", "Iron",
                                           "Laptop friendly workspace" ]
                                         }
                            }).pretty()

#### Ejercicios
ej 10. (lab 1 arrays) -> db.listingsAndReviews.find({ "reviews": { "$size":50 }, "accommodates": { "$gt":6 }})

ej 11. (lab 2 arrays) -> db.listingsAndReviews.find({"$and": [{"property_type": "House"}, {"amenities": {"$all": ["Changing table"]}}]}).count();

Quiz -> db.listingsAndReviews.find(
  { "amenities":
     { "$all": [ "Free parking on premises", "Wifi", "Air conditioning" ] },
    "bedrooms": { "$gte":  2 } }).pretty()

### Array operators and projection
db.collection.find({query}, {projection})
db.collection.find({query}, {field1: 1, field2: 1}) 
db.collection.find({query}, {field1: 0, field2: 0})

1 - Include the field.
0 - Exclude the field.
Use only 1s or only 0s.
La única vez que se pueden mezclar 0s y 1s, es cuando queremos excluir _id: 
db.collection.find({query}, {field1: 1, "_id": 0})

$elemMatch
{field: {"$elemMatch": {field: value}}}
Matches documents that containt an array field with at least one element that 
matches the specified query criteria.
oooorrr
Projects only the array elements with at least one element that matches the 
specified criteria.

#### Ejemplos
Find all documents with exactly 20 amenities which include all the amenities listed in the query array, and display their price and address:
db.listingsAndReviews.find({ "amenities":
        { "$size": 20, "$all": [ "Internet", "Wifi",  "Kitchen", "Heating",
                                 "Family/kid friendly", "Washer", "Dryer",
                                 "Essentials", "Shampoo", "Hangers",
                                 "Hair dryer", "Iron",
                                 "Laptop friendly workspace" ] } },
                            {"price": 1, "address": 1}).pretty()

Find all documents that have Wifi as one of the amenities only include price and address in the resulting cursor:
db.listingsAndReviews.find({ "amenities": "Wifi" },
                           { "price": 1, "address": 1, "_id": 0 }).pretty()

Find all documents that have Wifi as one of the amenities only include price and address in the resulting cursor, also exclude ``"maximum_nights"``. **This will be an error:*
db.listingsAndReviews.find({ "amenities": "Wifi" },
                           { "price": 1, "address": 1,
                             "_id": 0, "maximum_nights":0 }).pretty()

Get one document from the collection:
db.grades.findOne()

Find all documents where the student in class 431 received a grade higher than 85 for any type of assignment:
db.grades.find({ "class_id": 431 },
               { "scores": { "$elemMatch": { "score": { "$gt": 85 } } }
             }).pretty()

Find all documents where the student had an extra credit score:
db.grades.find({ "scores": { "$elemMatch": { "type": "extra credit" } }
               }).pretty()

#### Ejercicios
ej 12. (lab array and projection) -> db.companies.find({ "offices": { "$elemMatch": { "city": "Seattle" } }}).count()

Quiz array and projection -> db.companies.find({"funding_rounds": {"$size": 8}}, {"name": 1, "_id": 0})

### Array operators and sub-documents
MQL uses dot-notation to specify the address of nested elements in a document.

To use dot-notation in arrays, specify the position of the element in the array.

Syntax -> db.collection.find({"field.1.otherField.field.alsoAField": "value"})

#### Ejemplos
use sample_training

db.trips.findOne({ "start station location.type": "Point" })

db.companies.find({ "relationships.0.person.last_name": "Zuckerberg" }, 
// 0 = posición del elemento (el "i") / person = campo con un subdocumento / last_name = campo dentro de person / Zuckerberg = valor que estamos buscando
                  { "name": 1 }).pretty()

db.companies.find({ "relationships.0.person.first_name": "Mark",
                    "relationships.0.title": { "$regex": "CEO" } },
                  { "name": 1 }).count()


db.companies.find({ "relationships.0.person.first_name": "Mark",
                    "relationships.0.title": {"$regex": "CEO" } },
                  { "name": 1 }).pretty()

db.companies.find({ "relationships":
                      { "$elemMatch": { "is_past": true,
                                        "person.first_name": "Mark" } } },
                  { "name": 1 }).pretty()

db.companies.find({ "relationships":
                      { "$elemMatch": { "is_past": true,
                                        "person.first_name": "Mark" } } },
                  { "name": 1 }).count()

#### Ejercicios
ej 13. (Lab 1 sub-documents) -> db.trips.find({ "start station location.coordinates": { "$lt": -74 }}).count()

ej 14. (Lab 2) -> db.inspections.find({"address.city": {"$eq": "NEW YORK"}}).count();

Quiz -> db.listingsAndReviews.find({"amenities.0": "Internet"}, {"name": 1, "address": 1}).pretty()

### Chapter 4 IDE
#### Comparison
1 -> db.zips.find({"pop": {"$lt": 1000}}).count()
2 -> db.trips.find({"birth year": {"$gt": 1998}}).count() - db.trips.find({"birth year": {"$eq": 1998}}).count()
3 -> 
  db.routes.find({ "stops": { "$gt": 0 }}).pretty()
  db.routes.find({ "stops": { "$gte": 0 }}).pretty()

#### Logic
1 -> db.inspections.find({"$and": [{"result": {"$eq": "Out of Business"}}, {"sector": {"$eq": "Home Improvement Contractor - 100"}}]}).count()
2 -> db.zips.find({"$nor": [{"pop": {"gt": 100000}}, {"pop": {"$lt": 5000}}]}).count();
3 -> db.companies.find({"$or": [{"$and": [{"founded_year": 2004}, {"$or": [{"category_code": "web"}, {"category_code": "social"}]}]}, {"$and": [{"founded_month": 10}, {"$or": [{"category_code": "web"}, {"category_code": "social"}]}]}]}).count()

#### Expressive
db.companies.find({$expr: {"$eq": ["$permalink", "$twitter_username"]}}).count()

#### Array operators
1 -> db.listingsAndReviews.find({ "reviews": { "$size":50 }, "accommodates": { "$gt":6 }})
2 -> db.listingsAndReviews.find({"$and": [{"property_type": "House"}, {"amenities": {"$all": ["Changing table"]}}]}).count();

#### Array Operators and Projection
db.companies.find({ "offices": { "$elemMatch": { "city": "Seattle" } }}).count()

#### Array Operators and Sub-Documents
1 -> db.trips.find({ "start station location.coordinates": { "$lt": -74 }}).count()
2 -> db.inspections.find({"address.city": {"$eq": "NEW YORK"}}).count();

## CHAPTER 5
### Aggregation Framework
Works as a pipeline. It does not inherently modify or change the original data.
Statements are executed in order.

MQL -> Filter & update data.
Aggregation Framework -> $group -> compute & reshape data

$group operator -> Takes the incoming stream of data and siphons it into multiple distinct reservoirs. // Actúa como un filtro.


#### Ejemplos
Find all documents that have Wifi as one of the amenities. Only include price and address in the resulting cursor.
db.listingsAndReviews.find({ "amenities": "Wifi" },
                           { "price": 1, "address": 1, "_id": 0 }).pretty()

Using the aggregation framework find all documents that have Wifi as one of the amenities``*. Only include* ``price and address in the resulting cursor.
db.listingsAndReviews.aggregate([
                                  { "$match": { "amenities": "Wifi" } },
                                  { "$project": { "price": 1,
                                                  "address": 1,
                                                  "_id": 0 }}]).pretty()

Find one document in the collection and only include the address field in the resulting cursor.
db.listingsAndReviews.findOne({ },{ "address": 1, "_id": 0 })

Project only the address field value for each document, then group all documents into one document per address.country value.
db.listingsAndReviews.aggregate([ { "$project": { "address": 1, "_id": 0 }},
                                  { "$group": { "_id": "$address.country" }}])

Project only the address field value for each document, then group all documents into one document per address.country value, and count one for each document in each group.
db.listingsAndReviews.aggregate([
                                  { "$project": { "address": 1, "_id": 0 }},
                                  { "$group": { "_id": "$address.country",
                                                "count": { "$sum": 1 } } }
                                ])

#### Ejercicios
Lab -> db.listingsAndReviews.aggregate([{"$project": {"room_type": 1, "_id":0}},{"$group": {"_id": "$room_type"}}]);
    opción campus -> db.listingsAndReviews.aggregate([ { "$group": { "_id": "$room_type" } }])

Quiz -> aggregate() allows us to compute and reshape data in the cursor // aggregate() can do what find() can and more.

### Sort() and limit()
Get results in the order an quantity that we're looking for.

Sort() and limit() are cursor methods (like pretty() and count()). A cursor methods apply to the result.

db.zips.find().sort({"pop": 1, "city": -1})
1 == increasing // -1 == decreasing

limit() -> Specific number of results that best match the query

Use of sort() with limit() -> Use sort() first ==> cursor.limit().sort() === cursor.sort().limit()

#### Ejemplos
use sample_training
db.zips.find().sort({ "pop": 1 }).limit(1)
db.zips.find({ "pop": 0 }).count()
db.zips.find().sort({ "pop": -1 }).limit(1)
db.zips.find().sort({ "pop": -1 }).limit(10)
db.zips.find().sort({ "pop": 1, "city": -1 })

#### Ejercicios
Quiz 1 
  -> db.companies.find({"founded_year": {"$ne": null}}, {"name":1, "founded_year":1}).sort({"founded_year": 1}).limit(5)
  -> db.companies.find({"founded_year": {"$ne": null}}, {"name":1, "founded_year":1}).limit(5).sort({"founded_year": 1})

Quiz 2 -> db.trips.find({"birth year": {"$ne": ''}}, {"birth year": 1}).sort({"birth year": -1}).limit(1)

### Indexes
Make queries more efficient 

Index == Special data structure that stores a small portion of collection's data set
in an easy to traverse form

-> Single field index
Syntax -> db.collection.createIndex({"field": order})
order == 1 or -1

-> Compound index
db.collection.createIndex({"field": order, "field": order})

#### Ejemplos
db.trips.find({ "birth year": 1989 })
db.trips.find({ "start station id": 476 }).sort( { "birth year": 1 } )
db.trips.createIndex({ "birth year": 1 })
db.trips.createIndex({ "start station id": 476, "birth year": 1 })

#### Ejercicios
Quiz -> db.routes.createIndex({ "src_airport": -1 })

### Introduction to Data Modeling
A way to organize fields in a document to support your app performance and querying capabilities.

Rule: Data is stored in the way it's used.

- Data that is used together should be stored together.
- Envolving app implies an envolving data model.

#### Ejercicios
Quiz -> A way to organize fields in a document to support your app performance and querying capabilities.

### Upsert - Update or Insert?
Everything in MQL that is used to locate a document in a collection can also be
used to modify this document.

-> Update
db.collection.updateOne({queryToLocate}, {update})

-> Upsert == An hybrid of update and insert, it should only be used when it's needed. 
db.collection.updateOne({query}, {update}, {"upsert": true})

If upsert = true and there's a match, UPDATE the matched document.
If upsert = true and there's not a match, INSERT the matched document.

Upsert is good for conditional updates, in other cases is better set upset false and update the existing document or insert a new brand document.


#### Ejemplos
db.iot.updateOne({ "sensor": r.sensor, "date": r.date,
                   "valcount": { "$lt": 48 } },
                         { "$push": { "readings": { "v": r.value, "t": r.time } },
                        "$inc": { "valcount": 1, "total": r.value } },
                 { "upsert": true })

#### Ejercicios
Quiz 
  -> By default upsert is set to false.
  -> When upsert is set to true and the query predicate returns an empty cursor, the update operation creates a new document using the directive from the query predicate and the update predicate.
  -> When upsert is set to false and the query predicate returns an empty cursor then there will be no updated documents as a result of this operation. 

## CHAPTER 6
### Atlas Features - More Data Explorer
Performance advisory.
Aggregation builder. // permite exportar al lenguaje de nuestra app.
Anti-pattern advisory.
Advanced text search.

#### Ejercicios
Quiz 
  -> Export pipeline to a programming language.
  -> Syntax for each selected aggregation stage.
  -> A preview of the data in the pipeline at each selected stage.

### Atlas Products and Options
Acces manager -> group projects, teams and billing.
Realm -> for integrate mongodb into our app (web, mobile, other).
Charts -> allows us to create dynamic data visualizations.

#### Ejercicios
Quiz -> A product that helps you build visualizations of the data stored in your Atlas Cluster.

### What is MongoDB Compass?
Connection + Documents + Schema + Indexes.

#### Ejercicios
Quiz -> MongoDB's Graphical User Interface Product